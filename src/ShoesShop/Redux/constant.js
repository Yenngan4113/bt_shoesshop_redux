export const ADD_TO_CART = "Add to cart";
export const DELETE_ITEM = "Delete item";
export const REDUCE_QUANTITY = "Reduce quantity";
export const INSCREASE_QUANTITY = "Inscrease quantity";
export const MODAL_OK = "OK delete";
export const CLOSE_MODAL = "Close Modal";
