import { data } from "../Component/dataShoesShop";
import {
  ADD_TO_CART,
  CLOSE_MODAL,
  DELETE_ITEM,
  INSCREASE_QUANTITY,
  MODAL_OK,
  REDUCE_QUANTITY,
} from "./constant";

let initialState = {
  itemList: data,
  cart: [],
  isOpenModal: false,
  deletedItem: -1,
};

export const shoesReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((cartItem) => {
        return cartItem.id == payload.id;
      });
      if (index == -1) {
        let newItem = { ...payload, quantity: 1 };
        cloneCart.push(newItem);
      } else {
        cloneCart[index].quantity++;
      }
      state.cart = cloneCart;
      let dataJson = JSON.stringify(state);
      let dataState = JSON.parse(dataJson);
      return { ...dataState };
    }
    case DELETE_ITEM: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((cartItem) => {
        return cartItem.id == payload.id;
      });
      if (index != -1) {
        cloneCart.splice(index, 1);
      }
      state.cart = cloneCart;
      let dataJson = JSON.stringify(state);
      let dataState = JSON.parse(dataJson);
      return { ...dataState };
    }
    case REDUCE_QUANTITY: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((cartItem) => {
        return cartItem.id == payload.id;
      });
      if (index != -1 && cloneCart[index].quantity > 2) {
        cloneCart[index].quantity--;
      } else if (index != -1 && cloneCart[index].quantity == 2) {
        cloneCart[index].quantity--;
      } else {
        state.isOpenModal = true;
        state.deletedItem = index;
      }
      state.cart = cloneCart;
      let dataJson = JSON.stringify(state);
      let dataState = JSON.parse(dataJson);
      return { ...dataState };
    }
    case INSCREASE_QUANTITY: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((cartItem) => {
        return cartItem.id == payload.id;
      });
      if (index != -1) {
        cloneCart[index].quantity++;
      }
      state.cart = cloneCart;
      let dataJson = JSON.stringify(state);
      let dataState = JSON.parse(dataJson);
      return { ...dataState };
    }
    case MODAL_OK: {
      state.isOpenModal = false;
      let cloneCart = [...state.cart];
      cloneCart.splice(state.deletedItem, 1);
      state.cart = cloneCart;
      let dataJson = JSON.stringify(state);
      let dataState = JSON.parse(dataJson);
      return { ...dataState };
    }
    case CLOSE_MODAL: {
      state.isOpenModal = false;
      let dataJson = JSON.stringify(state);
      let dataState = JSON.parse(dataJson);
      return { ...dataState };
    }
    default:
      return state;
  }
};
