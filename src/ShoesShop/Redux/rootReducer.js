import { combineReducers } from "redux";
import { shoesReducer } from "./reducer.js";

export const rootReducer = combineReducers({
  shoesReducer,
});
