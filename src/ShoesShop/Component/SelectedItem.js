import { Modal } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  CLOSE_MODAL,
  DELETE_ITEM,
  INSCREASE_QUANTITY,
  MODAL_OK,
  REDUCE_QUANTITY,
} from "../Redux/constant";

class SelectedItem extends Component {
  render() {
    let total = this.props.selectedItem.reduce((accumulator, currentValue) => {
      return accumulator + currentValue.quantity * currentValue.price;
    }, 0);
    return (
      <div>
        <Modal
          title="Confirm"
          visible={this.props.isOpenModal}
          onOk={() => {
            this.props.handleOK();
          }}
          onCancel={() => {
            this.props.handleCloseModal();
          }}
        >
          <p>Are you sure to delete this item</p>
        </Modal>
        <table className="table text-center ">
          <thead>
            <tr>
              <td>ID</td>
              <td>Product</td>
              <td>Price</td>
              <td>Quantity</td>
              <td>Total</td>
              <td></td>
            </tr>
            <tr>
              <td
                colSpan={3}
                className="text-left font-weight-bold"
                style={{ fontSize: "18px" }}
              >
                Total
              </td>
              <td
                colSpan={3}
                className="font-weight-bold"
                style={{ fontSize: "22px" }}
              >
                {new Intl.NumberFormat("de-DE").format(total)}$
              </td>
            </tr>
          </thead>
          <tbody>
            {this.props.selectedItem.map((item) => {
              return (
                <tr>
                  <td>{item.id}</td>
                  <td>{item.name}</td>
                  <td>{item.price}$</td>
                  <td className="row py-1 justify-content-center align-items-center">
                    <button
                      className="btn btn-danger mr-1 h-100 py-1"
                      onClick={() => {
                        this.props.reduceQuantity(item);
                      }}
                    >
                      {item.quantity == 1 ? "Del" : "-"}
                    </button>
                    {item.quantity}
                    <button
                      className="btn btn-success ml-1"
                      onClick={() => {
                        this.props.increaseQuantity(item);
                      }}
                    >
                      +
                    </button>
                  </td>
                  <td>
                    {new Intl.NumberFormat("de-DE").format(
                      item.quantity * item.price
                    )}
                    $
                  </td>
                  <td className="py-1">
                    <button
                      className="btn btn-danger "
                      onClick={() => {
                        this.props.handleDeleteItem(item);
                      }}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedItem: state.shoesReducer.cart,
    isOpenModal: state.shoesReducer.isOpenModal,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    handleDeleteItem: (item) => {
      dispatch({
        type: DELETE_ITEM,
        payload: item,
      });
    },
    reduceQuantity: (item) => {
      dispatch({
        type: REDUCE_QUANTITY,
        payload: item,
      });
    },
    increaseQuantity: (item) => {
      dispatch({
        type: INSCREASE_QUANTITY,
        payload: item,
      });
    },
    handleOK: () => {
      dispatch({
        type: MODAL_OK,
      });
    },
    handleCloseModal: () => {
      dispatch({
        type: CLOSE_MODAL,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectedItem);
