import React, { Component } from "react";
import { connect } from "react-redux";

import ShoesItem from "./ShoesItem";

class ShoesList extends Component {
  render() {
    return (
      <div className="row">
        {this.props.Shoesdata.map((item) => {
          return <ShoesItem Item={item} />;
        })}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { Shoesdata: state.shoesReducer.itemList };
};

export default connect(mapStateToProps)(ShoesList);
