import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART } from "../Redux/constant";

class ShoesItem extends Component {
  render() {
    let { image } = this.props.Item;
    let product = this.props.Item;
    let { name } = this.props.Item;
    let { description } = this.props.Item;
    let { price } = this.props.Item;
    return (
      <div className="col-4">
        <div className="card mb-3" style={{ height: "670px" }}>
          <img className="card-img-top" src={image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <span>{price}$</span>
            <p className="card-text my-2 mb-4">{description}</p>
            <a
              href="#"
              className="btn btn-warning"
              onClick={() => {
                this.props.handleAddToCart(this.props.Item);
              }}
            >
              Add to cart
            </a>
          </div>
        </div>
      </div>
    );
  }
}

const mapDisPatchToProps = (dispatch) => {
  return {
    handleAddToCart: (product) => {
      return dispatch({
        type: ADD_TO_CART,
        payload: product,
      });
    },
  };
};

export default connect(null, mapDisPatchToProps)(ShoesItem);
