import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import SelectedItem from "./SelectedItem";
import ShoesList from "./ShoesList";

class BT_main extends Component {
  render() {
    let totalQuantity = this.props.selectedItem.reduce(
      (accumulator, currentValue) => {
        return accumulator + currentValue.quantity;
      },
      0
    );
    return (
      <Fragment>
        <h1 className="bg-dark text-white text-center">SHOES SHOP APP</h1>
        <div className="row">
          <div className="col-7">
            <ShoesList />
          </div>
          <div className="col-5">
            <h5 className="text-right text-danger mr-2">
              Giỏ hàng : {totalQuantity}
            </h5>
            {this.props.selectedItem.length != 0 && <SelectedItem />}
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedItem: state.shoesReducer.cart,
    isOpenModal: state.shoesReducer.isOpenModal,
  };
};
export default connect(mapStateToProps)(BT_main);
