import logo from "./logo.svg";
import "./App.css";
import { Fragment } from "react";
import BT_main from "./ShoesShop/Component/BT_main";
import "antd/dist/antd.css";

function App() {
  return (
    <Fragment>
      <BT_main />
    </Fragment>
  );
}

export default App;
